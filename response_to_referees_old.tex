\documentclass[prl,aps,onecolumn,groupaddress]{revtex4-1}
\usepackage{latexsym}
\usepackage{verbatim}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{mathrsfs}
\usepackage{float}
\usepackage{framed}
\usepackage{comment}
\usepackage[usenames, dvipsnames]{color}
\usepackage{mathtools}
\usepackage{slashed}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{wasysym}

% \usepackage{natbib}
% \bibliographystyle{abbrvnat}
% \setcitestyle{authoryear,round} %Citation-related commands

\newenvironment{letterstyle}{%
  \setlength{\parindent}{0pt}%
  \setlength{\parskip}{1.5em}%
}{%
  \setlength\parindent{3em}%
  \setlength{\parskip}{0em}%
}%

\newenvironment{referee}{%
  \noindent\begin{leftbar}\begin{quotation}\begin{letterstyle}%
}{%
  \end{letterstyle}\end{quotation}\end{leftbar}%
}%

\newenvironment{response}{%

  \par%
  \noindent\textbf{Response:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}

\newenvironment{manuscript}{%

  \par\color{blue}%
  \noindent\textbf{Changes in the manuscript:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}

\newcommand{\todo}{ \textbf{TODO} }

\begin{document}
\begin{letterstyle}
Dear editor,

On behalf of both of us, the authors, I thank you for the detailed
comments and the kind words of encouragement. As this is my first
submission, and if successful my first published research, I hope that
the flaws of the current submission are attributed to my inexperience.

The referees have provided detailed feedback on the paper. The summary
conclusion is that with a few alterations the present work shall be
suitable for publication in Journal of Physics A. The second referee's
terser comments highlight what the first referee explains in more
detail.

The major comments are as follows: include a quantitative validation
of the result into the paper, rather than leave it attached to the
code accompanying the submission, as well as provide a discussion of
the role of determinism in the obtained result.

Both are valid critiques that we worked hard to address.  Though we
have carried out a Bayesian validation of the analytical approximation
as compared to the numerical solution, it was omitted from the final
paper, because unfortunately Bayesian analysis is still niche, and to
properly explain the results of the validation would require a large
detour in an already large volume of text. We agree that this is
necessary, and while we planned submitting the validation as part of a
follow-up publication, we shall reinstate the relevant sections.

The second aspect is a little more complex. It is true that the model
that we've chosen can only appear stochastic, with all of the
underlying dynamics being fully deterministic. It is debatable,
whether or not a fundamentally non-deterministic model could exist
without accounting for quantum effects, and a relativistic treatment
of quantum effects, famously lead to the development of the Quantum
Field theory. We fully agree that a more accurate approach would
involve a field-theoretical description, and that the resultant
equation would be far more applicable to the high-temperature
behaviour. However, said observation does not invalidate the result
presented here: a simple mechanical model that can be used to derive a
deterministic form of the generalised Langevin equation in the
Newtonian limit can be extended to produce a similar equation under
suitable approximations and given a relativistic treatment. While
calling it \emph{the} relativistic Langevin equation is problematic,
it is certainly \emph{a} relativistic Langevin equation. We have
made the statement of the paper as a whole more precise, hopefully
addressing the concerns of the referee.

What follows is a detailed response to the comments left by the
referees.

Sincerely\\
Aleksandr Petrosyan

\end{letterstyle}
\pagebreak

\begin{referee}
  (1) The structure of the manuscript and the writing style are to my
  opinion problematic. I got lost several times along the way, mostly
  because the authors do not clearly explain at the beginning what is
  their strategy and what they are going to do. For instance, a major
  part of the derivation is based on the solution that Mathematica
  finds, and then this starting point is progressively peeled off to
  obtain a more compact form for the solution. This is a perfectly
  fine method, but the reader discovers the intent of the authors as
  the text goes, and it is often hard to pick up the pieces. I would
  have liked a clear paragraph in the introduction, and at the
  beginning of sections III and IVwhich clearly explains what steps
  the authors are going to follow. As the manuscript is quite long and
  technical, this is to my opinion essential.

\end{referee}

\begin{response}
    We thank the referee for pointing this out. We have made the requested changes. In particular at the end of Secion~IV A we have added an outline of the entire process of solving the equation numerically.
\end{response}

\begin{referee}
  \ldots Paragraph IV.C is in particular extremely hard to digest. While I
  fully understand that the full solution can not be displayed in the
  main text and has to be relegated to the supplemental material, the
  fact that there is only inline text in this section is not easy to
  follow. Some of the explanations should, I think, go straighter to
  the point, which might imply a bit more written math, while some of
  the very technical points could be relegated to the supplemental
  material. In the current state of the manuscript, it is for instance
  hard to grasp the very definition/meaning of some of the central
  quantities like $\bar{\xi}$, $\bar{\omega}$, $\hat{\xi}$ and $\hat{\omega}$
  (apart from being integration constants).
\end{referee}

\begin{response}
    We have addressed the issue by an extensive rewrite Sec~IV C. Appendix D addresses the question as regarding the integration constants.
\end{response}

\begin{referee}
  2) There seems to be some confusion with the very foundation of the
  Langevin equation and its relation to the Caldeira-Leggett
  model. While this model is important because of its compactness and
  practicability in order to model a heat bath, the scope of the
  Langevin Equation is actually much wider than the CL model. In
  classical systems, the structure of the Langevin equation can
  actually be derived from first principles under very few assumptions
  (only that the microscopic dynamics is deterministic) using
  e.g. projection operator techniques such as the Mori-Zwanzig
  formalism. The Caldeira-Leggett model is ``just''  a case in which
  the dynamics of the bath can be decoupled from the one of the tagged
  particle. I find that the authors sometimes tend to overestimate the
  applicability of this model.

\end{referee}

\begin{response}
  We thank the referee for the observation. Indeed, the
  Caldeira-Legett model is one we have chosen because of the ease of
  extension. We have added a paragraph clarifying this point to Section II.
\end{response}

\begin{referee}
  In addition, it should be stressed that the model is fully
  deterministic, while the ``true'' Langevin Equation is
  stochastic. The resulting equation of the Caldeira-Leggett can only
  be interpreted as effectively stochastic under some
  conditions. While these conditions are well-known in the classical
  case, I was not convinced by the discussion about this aspect in the
  relativistic case.  For instance, the validity of the stochastic
  nature of the equation in a classical process is often discussed in
  terms of timescale separation, i.e. the stochastic interpretation is
  valid if the main particle has a much slower dynamics that the one
  of the bath particles. This argument of timescale separation is much
  less straightforward to carry over in the relativistic case, which
  could be a nice aspect for the authors to discuss. In short, I think
  the authors should explain more why we can interpret their equation
  (27), which is intrinsically deterministic, as a stochastics
  process. In the current state of the manuscript, the reader might
  have the impression that the resulting ``Langevin'' equation is
  intrinisically stochastic.
\end{referee}

\begin{response}
  We thank the referee for the comment. Indeed, one would assume that
  the stochastic interpretation of our Langevin equation follows from
  rather general assumptions about the behaviour of large systems. In
  addition to discussing the correlation properties of the stochastic
  force, we have included a discussion in the section
  concerning the Galilean relativistic Generalized Langevin
  equation to the end of Section II. We also included a section dedicated to discussions of stochasticity at the beginning of the "Discsussion" section.
\end{response}

\begin{referee}
  (1) Figure 1 is not easy to read as both curves superimpose. Maybe
  the authors could plot the difference between blue and orange in an
  inset?
\end{referee}

\begin{response}
  We removed Figure 1, as it served no purpose in the revised text.
\end{response}

\begin{referee}
  (2) where does the ansatz (23) come from ? I do not understand this
  particular shape
\end{referee}

\begin{response}
  It is the lowest order term in the MacLaurin series expansion of the
  \(\xi(t, \mathbf{x})\) function, that has the requisite symmetries,
  particularly that of spatial inversion symmetry. Relevant discussion added to Section IV E
\end{response}

\begin{referee}
  (3) Can one explain the relation $\bar{\omega} = gamma^{-3/4}$? Is it
  always verified when you change some parameters (e.g.~the initial
  conditions)?
\end{referee}

\begin{response}
  This value was provided to illustrate a point: that the shift in frequency could not be explained just by having extra Lorentz factors and a form of time dilation. We have clarified this point in the revised text in the penultimate paragraph of Section IV E.
\end{response}

\begin{referee}
  (4) The discussion part lacks a paragraph on the newly discovered
  term \(\mathbf{F}_{r}\). I think that this new term is one of the
  most important and exciting results of the manuscript, and I think
  it is not enough discussed.
\end{referee}

\begin{response}
  The newly discovered term is discussed  in Section VII. Section VIIB considers its behaviour in the non-relativistic limit \(F_r \rightarrow 0\), Section VIIC --- how its presence affects the symmetries of the Generalised Langevin equation. Section VIIE is concerned with how the presence of the restoring force may be reconciled with the Fluctuation dissipation relations.

  In our opinion a more complete exploration the implications of the extra term would warrant another full paper, which   we are in the process of preparing. Unfortunately, the full discussion is closely related to the relativistic equivalents of the Fluctuation-dissipation relations, which we have not yet explored sufficiently.
\end{response}

\begin{referee}
What about its staistical properties for instance?
\end{referee}

\begin{response}
  unfortunately, not much, other than that it likely participates in
  the relativistic version of the fluctuation-dissipation relations.
  The main issue that gives rise to the difficulty, is that the
  function's existence is predicated on a specific value of the
  correction parameter \(\xi(t, \mathbf{x})\), which we have only
  successfully evaluated for a particular set of boundary and initial
  conditions.  The odds of conditions like the ones we discussed
  arising naturally are thermodynamically insignificant.

  It is work that needs to be done, however.  We expect that having a
  broader discussion of the implications of the current submission as
  a follow-up publication would provide us suitable time to
  investigate the behaviour of \(\mathbf{F}_{r}\) under many more
  conditions to be able to talk about it in more precise language.
\end{response}
\end{document}
