\documentclass[prl,aps,onecolumn,groupaddress]{revtex4-1}
\usepackage{latexsym}
\usepackage{verbatim}
\usepackage{multirow}
\usepackage{amsmath}
\usepackage{mathrsfs}
\usepackage{float}
\usepackage{framed}
\usepackage{comment}
\usepackage[usenames, dvipsnames]{color}
\usepackage{mathtools}
\usepackage{slashed}
\usepackage{physics}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{subfigure}
\usepackage{hyperref}
\usepackage{bbold}
\usepackage{wasysym}

\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,round} %Citation-related commands

\newenvironment{letterstyle}{%
  \setlength{\parindent}{0pt}%
  \setlength{\parskip}{1.5em}%
}{%
  \setlength\parindent{3em}%
  \setlength{\parskip}{0em}%
}%

\newenvironment{referee}{%
  \noindent\begin{leftbar}\begin{quotation}\begin{letterstyle}%
}{%
  \end{letterstyle}\end{quotation}\end{leftbar}%
}%

\newenvironment{response}{%

  \par\color{blue}%
  \noindent\textbf{Response:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}

\newenvironment{manuscript}{%

  \par\color{blue}%
  \noindent\textbf{Changes in the manuscript:}

  \begin{letterstyle}
}{%
  \end{letterstyle}
  \par%
}


\begin{document}
\section{Response for manuscript DC13120}
\begin{letterstyle}

  Dear Editor,

  We are writing to resubmit the manuscript BX13886 entitled
  ``Relativistic Langevin Equation derived from a particle-bath
  Lagrangian" to Phys. Rev. D as a regular article. We would like to
  thank the Referee for assessing the manuscript, and
  for providing useful guidance on how to improve the paper.

  We read the critiques of the Referee and would like to offer our
  response to the criticisms and suggestions of the Referee.  We have
  implemented all the changes requested by the Referees such that our
  extensively revised paper now hopefully meets criteria to be
  published in PRD.

  In particular, we made an effort to provide the derivations
  requested by the Referee:

  (1) we now provide an explicitly Lorentz-covariant form of our
  starting Lagrangian and of the resulting Langevin equation;

  (2) we completed the characterization of the noise term
  $\mathbf{F}'_{p}$ and now show in the manuscript that this term
  possess all the requirements of a stochastic force, and provide a
  generalized relativistic fluctuation-dissipation theorem, which
  comprises the standard non-relativistic term (that was derived by
  Zwanzig with the particle-bath approach) and a new relativistic
  correction term which vanishes in the limit of non-relativistic
  velocities.

  We are hopeful that the current extensively revised version of the
  manuscript addresses all the recommendations and critiques of the
  Referee and is now suitable for publication in PRD.

  Best regards,\\
  \textit{The authors}
\end{letterstyle}


% 4) With regard to the Second referee's technical comment ``1. The
% paper is considerably revised to include a more detailed analysis of
% all microscopic parameters,", we have added relevant results and
% parameter analysis in the Appendix.

\newpage
\section{Response to the Reviewer}
\begin{referee}
  In this work, a ``Langevin'' equation is derived from a
  ``relativistic" Lagrangian using the Euler-Lagrange equation. The
  authors then found that the derived equation has an additional term
  which does not appear in the standard relativistic Langevin
  equation. Because of the reasons explained below, however, the
  approach developed in the paper does not use the theory of
  relativity appropriately. Moreover the derived equation is
  deterministic and hence the Langevin equation is not yet derived. In
  conclusion, I do not recommend this work for publication in the
  present form.
\end{referee}

\begin{response}
  We thank the Referee for these useful comments and criticism. The
  Referee makes two main critiques to our work: one is about the lack
  of explicit Lorentz-covariant form of our equations, the other is
  about the allegedly deterministic character of the noise.

  We agree with the first point, and we made a substantial effort in
  the revision to provide explicitly Lorentz-covariant forms in the
  revised manuscript. We find that the second remark is not entirely
  correct, because the stochastic character of the noise term as
  derived in our paper with the particle-bath approach was already
  demonstrated (in the non-relativistic case) in the acclaimed work of
  \cite{Zwanzig1973} and is, by now, textbook
  material~\cite{zwanzig2001nonequilibrium}. However we are grateful
  to the Referee for motivating us to show the stochastic character of
  the noise term \(\mathbf{F}'_p\) explicitly and to provide a new
  relativistic fluctuation-dissipation theorem with a relativistic
  correction, which correctly recovers \citeauthor{Zwanzig1973}'s
  result in the non-relativistic limit.

  We added these considerations and derivations in the new version,
  which show that the stochastic term possesses all the features
  required for the noise, in terms of both having zero-average and of
  producing a fluctuation-dissipation type relation in accordance with
  previous works that used the particle-bath framework.
\end{response}


\begin{referee}
  \noindent1) The Lorentz covariance

  It is well-known that the interaction between relativistic particles
  cannot be expressed using a potential because a force given by a
  potential describes action at a distance. Thus the interaction is
  normally considered to be mediated by a field such as the case of
  the gauge interaction. Nevertheless such a potential interaction is
  considered in the Lagrangian~(9) and thus it is difficult to accept
  the appropriateness of the obtained result. To justify such an
  extraordinary approach, the authors should show the consistency of
  their theory with the theory of relativity more carefully. First,
  the Lorentz invariance of the action obtained from the
  Lagrangian~(9) should be shown. Note also that the Lagrangian~(9)
  has a parameter $t_i$ but this is not defined in the paper (even in
  App.~A.). Second, the Lorentz covariance of the following equations
  should be shown; the Euler-Lagrange equations (11a,11b), Eq.~(15),
  Eq.~(21) and Eq.~(23).
\end{referee}


\begin{response}
  We thank the Referee for raising the question of action at a
  distance. We should start by noting that there are two potentials to
  which the Referee's comment about action at a distance can be
  appropriate. The first such potential is the ``external'' potential
  acting only on the tagged particle. This force had been used
  primarily as a shorthand, without assuming any underlying mechanism;
  we agree that in the case of special relativity further discussion
  of this term is needed. To this end, in the revised submission we
  have added Eq.~(11), to model the external interaction field as
  analogous to electromagnetism, and further demonstrated that the
  Euler-Lagrange equations~(15), do not change significantly from
  their form that assumed a static potential, up to a redefinition of
  what the
  external force \(\mathbf{F}_\text{ext}\) is. \\

  The second potential corresponds to the coupling interaction between
  bath eigenmodes and the tagged particle. This interaction is
  proportional to the Lorentz invariant interval separating
  interaction events. As constructed, we assume, in a standard way,
  that the interaction is mediated by a massless field (i.e.~by
  massless gauge bosons), because this produces simpler Euler-Lagrange
  equations~(15). Hence, this is also fully correct and consistent
  with special relativity.


  In general, we do not completely agree that a conservative static
  potential like $V(\mathbf{x})$ is always outlawed in a relativistic
  context. There is by now an extensive literature on this topic.
  Several counterexamples can be found in textbooks,
  e.g.~\citealt[``Classical
  mechanics''][pp. 331---332]{goldstein1980classical}, or
  \citealt[``The theory of action-at-a-distance in relativistic
  particle dynamics''][]{Kerner1972TheTO}. Specifically, the harmonic
  oscillator potential in special relativity, is discussed in
  \citealt[``Classical mechanics''][page 324]{goldstein1980classical}.

  Another way to reassure the readers that the stationary potential
  \(V(\mathbf{x})\) is acceptable, is by reminding the reader that
  interaction is between the potential and the tagged particle, and
  hence no action-at-a-distance takes place. It would be if
  interaction with the potential propagated to the source of the
  potential \(V(\mathbf{x})\) instantaneously. In our case, however,
  the effect that the tagged particle would have on the source of
  \(V(\mathbf{x})\) would be so small to be negligible, thus
  \(V(\mathbf{x})\) remaining stationary does not constitute
  faster-than-light propagation of the interaction. Thus, we could
  have retained the time- and velocity-independent form of the
  potential \(V(\mathbf{x})\), and produced results consistent with
  special relativity.

  We, however, agree with the comment in that an explicit mediation of
  a familiar (electreomagnetic) field would be easier to accept for
  the reader, and this does not significantly affect the subsequent
  calculations and our final results. Therefore we followed the
  Referee's suggestion and we rephrased the text accordingly in the
  new version.

  We are grateful to the Referee for raising the issue of explicit
  Lorentz-covariant form of our model.  We have modified the
  formulation and presentation of our model and have put it in
  explicitly Lorentz-covariant form in the added sub-Section (VII.B) on
  page 13. In that section we demonstrate that the action
  corresponding to Eq.~(13) is Lorentz invariant, and address the
  Lorentz covariance of the equations for which said covariance does
  not follow from standard theorems: Eq.~(27) leading up to
  Eq.~(33). Please also see the new text and equations added on page 4
  and 5 preceding Eq.~(13), which provides our Lagrangian in fully
  Lorentz-covariant form.

  The parameter \(t_i\) has now been officially declared in the revised
  version: Eq.~(12) and preceding paragraphs show that it is the
  temporal component of the interaction events that constitute the
  mediator of the eigenmode-tagged particle interaction.\\
\end{response}

\begin{manuscript}
  We added a brief discussion about the potential term
  \(V(\mathbf{x})\) and the issue of action-at-a-distance.

  We added new text and equations in several places to put our
  formalism in explicitly Lorentz-covariant form. A whole new Section
  (VII.B) leading to the new Eq.~(33), which represents our
  relativistic Langevin equation written in Lorentz-covariant form,
  has been added.

  Note that due to the aforementioned additions, the equation
  numbering has shifted in the revised version.
\end{manuscript}


\begin{referee}
  2) Langevin equation

  The derivation of the Langevin equation is the main purpose of this
  paper. However, as is mentioned below Eq. (22b), the authors
  consider that the noise term (stochastic force) in their equation is
  still deterministic. That is, the derivation of the relativistic
  Langevin equation has not yet finished. The stochasticity of the
  noise term should be introduced and the correlation properties
  should be studied.  The authors then should pay attention to the
  difference between the additive and multiplicative noises in their
  theory. Moreover, to interpret the derived equation as the Langevin
  equation, the authors should derive the Kramers (Fokker-Planck)
  equation from their equation and confirm that the stationary
  solution of the Kramers equation is given by the Juettner
  distribution. Because of the additional term found in this paper,
  the derived equation may not describe the thermalization process.

\end{referee}



\begin{response}
  We thank the Referee for motivating us to provide a more complete
  demonstration of the noise properties of the stochastic force
  $\mathbf{F}'_{p}$ (see more on this below) and to provide the form
  of the relativistic fluctuation-dissipation theorem. We provided
  these further demonstrations in the revised paper, as discussed
  further below.


  Firstly, we'd like to clarify our interpretation of the Referee's
  comment. Classical mechanics is a fully deterministic theory; A
  deterministic treatment is possible whenever knowing the full
  initial conditions of a closed system permits prediction of the
  system's full state arbitrarily far into the future, with arbitrary
  precision. In statistical physics, however, the number of degrees of
  freedom is so large that it is effectively impossible to keep track
  of the dynamical evolution of all of them.  In both our and
  \citeauthor{Zwanzig1973}'s treatment, the equation would be
  deterministic if we could exactly know the initial conditions for
  all bath oscillators. Hence, the referee's comment does not refer to
  determinism in the sense in which e.g.~Laplace did. Instead, since
  knowing the initial conditions of the bath oscillators is
  unfeasible, to address the comment would mean to show that the
  stochastic force, has properties of thermal noise. Namely, one must
  look at time correlation functions.

  % However, what the Referee wrote is not entirely correct: we never
  % wrote below Eq. 22(b) that the noise is ``deterministic'', and, in
  % fact, it is not deterministic. We wrote: ``Here, exactly like in
  % the original Galilean derivation, the stochasticity of the force
  % follows from ``ignorance'' about the boundary conditions [1].  Had
  % we not coarse-grained away the information about the full
  % microstate of the system, the stochastic force would appear fully
  % deterministic [1]'' This is fundamentally different.

  In our derivation we follow exactly the same strategy.  First note
  that our derivation of the stochastic term is fully in line with the
  original derivation \citep{Zwanzig1973} for the non-relativistic
  case.  The noise that we derive \(\mathbf{F}'_{p}\) has all the
  characteristics of the noise \(\mathbf{F}_{p}\) for the generalized
  Langevin equation originally derived by \citeauthor{Zwanzig1973},
  which is now textbook material \cite*[``Nonequilibrium Statistical
  Mechanics''][page 22-23]{zwanzig2001nonequilibrium}. In our
  treatment, we obtain an additional relativistic correction term
  \(\mathbf{F}'_{r}\) as shown and discussed in the revised version of
  our paper.  Following \cite{Zwanzig1973}'s method, it can be exactly
  proved that the stochastic force $\mathbf{F}'_{p}$ possesses all the
  required features of stochastic noise, i.e.~it has zero time
  average.  Furthermore its ensemble-averaged time-correlation
  function reproduces a fluctuation-dissipation theorem, i.e.~it's
  proportional to a function of the type $K(\tau-\tau')$, which
  represents the friction kernel, plus relativistic correction terms
  which correctly vanishes in the appropriate limit. We included these
  steps and this discussion in the revised version.

  We highly appreciate the Referee's suggestion regarding the
  derivation of the Kramers (Fokker-Planck) equation corresponding to
  our Langevin equation. This considerable and non-trivial task falls
  outside the scope of the current paper. It is already 21 pages long in two-column format,
  and a complete treatment of the Fokker-Planck formalism would be
  equally long. Thus we plan on working on this derivation and on
  presenting it in forthcoming papers.\\
\end{response}

\begin{manuscript}
  We added a new discussion, on pages 12 and 17 of the revised
  submission, that shows that the stochastic force of our Langevin
  equation has zero average ${\langle \mathbf{F}'_{p}(t) \rangle=0}$,
  which thus qualifies it as a noise term. Furthermore, we added further analysis which shows that the time-correlation function
  satisfies a fluctuation-dissipation theorem of the type
  ${\langle\langle \mathbf{F}_{p}(\tau)\mathbf{F}_{p}(\tau') \rangle =
    k_{B}T K(\tau-\tau') + \text{Corr}}$, where the correction term is
  of relativistic origin, and it correctly vanishes in the
  non-relativistic limit of low velocities. Furthermore, the correction term is shown to be
  related to the time-correlation function of the new and emergent
  (relativistic-only) restoring force $\mathbf{F}'_{r}$, which also
  vanishes in the non-relativistic limit.
  We believe that these are important findings which add further value, novelty and robustness to our theory.
\end{manuscript}

\begin{referee}
  \noindent{}Minor comments


  \noindent{}A) Eq. (28) is wrong. This is not normalizable.

\end{referee}

\begin{response}
  Thank you for bringing this problem to our attention. This was a
  minor typo in the previous version of the paper: a minus sign was
  missing in the Boltzmann factor. We have fixed this typo in the
  revised version of the paper.
\end{response}



\begin{referee}
  B) Reference [59] is inadequate to cite here. The generalized
  fluctuation-dissipation theorem is already discussed in Koide and
  Kodama, Phys. Rev. E83, 061111 (2011).

\end{referee}

\begin{response}
  Thank you for bringing this important reference to our attention. We
  have cited and mentioned the generalized fluctuation-dissipation
  theorem discussed by Koide and Kodama in the revised version of our
  paper.
\end{response}

\bibliography{bibliography}
\end{document}
